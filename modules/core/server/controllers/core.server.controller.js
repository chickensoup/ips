'use strict';
var fs = require('fs');

/**
 * Render the main application page
 */
exports.renderIndex = function (req, res) {
  res.render('modules/core/server/views/index', {
    user: req.user || null
  });
};

exports.uploadFile = function (req, res) {
  var data = req.body.file;
  var base64Data = data.replace(/^data:image\/png;base64,/, '');
  base64Data = base64Data.replace(/^data:image\/jpeg;base64,/, '');
  base64Data += base64Data.replace('+', ' ');
  var binaryData = new Buffer(base64Data, 'base64').toString('binary');
  var fileName = new Date().getTime().toString();
  fs.writeFile('public/img/maps/' + fileName, binaryData, 'binary', function (err) {
    if (err) {
      console.log(err);
    }
    res.send(fileName);
  });
};

/**
 * Render the server error page
 */
exports.renderServerError = function (req, res) {
  res.status(500).render('modules/core/server/views/500', {
    error: 'Oops! Something went wrong...'
  });
};

/**
 * Render the server not found responses
 * Performs content-negotiation on the Accept HTTP header
 */
exports.renderNotFound = function (req, res) {

  res.status(404).format({
    'text/html': function () {
      res.render('modules/core/server/views/404', {
        url: req.originalUrl
      });
    },
    'application/json': function () {
      res.json({
        error: 'Path not found'
      });
    },
    'default': function () {
      res.send('Path not found');
    }
  });
};
