'use strict';

// Maps controller
angular.module('maps').controller('MapsController', ['$scope', '$http', '$stateParams', '$location', 'Authentication', 'Maps',
  function ($scope, $http, $stateParams, $location, Authentication, Maps) {
    $scope.authentication = Authentication;

    // Create new Map
    $scope.create = function (isValid) {
      $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'mapForm');

        return false;
      }

      // Create new Map object
      var map = new Maps({
        title: this.title,
        picture: this.picture,
        width: this.width,
        height: this.height,
        offsetX: this.offsetX,
        offsetY: this.offsetY,
        imgWidth: this.imgWidth,
        imgHeight: this.imgHeight
      });

      // Redirect after save
      map.$save(function (response) {
        $location.path('maps/' + response._id);

        // Clear form fields
        $scope.title = '';
        $scope.content = '';
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };

    // Remove existing Map
    $scope.remove = function (map) {
      if (map) {
        map.$remove();

        for (var i in $scope.maps) {
          if ($scope.maps[i] === map) {
            $scope.maps.splice(i, 1);
          }
        }
      } else {
        $scope.map.$remove(function () {
          $location.path('maps');
        });
      }
    };

    // Update existing Map
    $scope.update = function (isValid) {
      $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'mapForm');

        return false;
      }

      var map = $scope.map;
      if ($scope.picture) {
        map.picture = $scope.picture;
      }

      map.$update(function () {
        $location.path('maps/' + map._id);
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };

    // Find a list of Maps
    $scope.find = function () {
      $scope.maps = Maps.query();
    };

    // Find existing Map
    $scope.findOne = function () {
      $scope.map = Maps.get({
        mapId: $stateParams.mapId
      });
    };

    $scope.uploadPicture = function () {
      $http.post('/upload', {file: $scope.fileUpload})
        .then(function (response) {
          console.log(response);
          $scope.picture = response.data;
        }, function (response) {
          console.log('errrrrrr');
          console.log(response);
        });
    };
  }
]);
