'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var MapSchema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  title: {
    type: String,
    default: 'default-map',
    trim: true,
    required: 'Title cannot be blank'
  },
  picture: {
    type: String,
    default: 'default-map.png'
  },
  width: {
    type: Number,
    default: 0
  },
  height: {
    type: Number,
    default: 0
  },
  offsetX: {
    type: Number,
    default: 0
  },
  offsetY: {
    type: Number,
    default: 0
  },
  imgWidth: {
    type: Number,
    default: 0
  },
  imgHeight: {
    type: Number,
    default: 0
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  }
});

mongoose.model('Map', MapSchema);
