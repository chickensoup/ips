'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  MapModel = mongoose.model('Map'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * Create a map
 */
exports.create = function (req, res) {
  var map = new MapModel(req.body);
  map.user = req.user;

  map.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(map);
    }
  });
};

/**
 * Show the current map
 */
exports.read = function (req, res) {
  res.json(req.map);
};

/**
 * Update a map
 */
exports.update = function (req, res) {
  var map = req.map;

  map.title = req.body.title;
  map.picture = req.body.picture;
  map.width = req.body.width;
  map.height = req.body.height;
  map.offsetX = req.body.offsetX;
  map.offsetY = req.body.offsetY;
  map.imgWidth = req.body.imgWidth;
  map.imgHeight = req.body.imgHeight;

  map.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(map);
    }
  });
};

/**
 * Delete an map
 */
exports.delete = function (req, res) {
  var map = req.map;

  map.remove(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(map);
    }
  });
};

/**
 * List of Maps
 */
exports.list = function (req, res) {
  MapModel.find().sort('-created').populate('user', 'displayName').exec(function (err, maps) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(maps);
    }
  });
};

/**
 * Map middleware
 */
exports.mapByID = function (req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Map is invalid'
    });
  }

  MapModel.findById(id).populate('user', 'displayName').exec(function (err, map) {
    if (err) {
      return next(err);
    } else if (!map) {
      return res.status(404).send({
        message: 'No map with that identifier has been found'
      });
    }
    req.map = map;
    next();
  });
};
