'use strict';

var mapsPolicy = require('../policies/maps.server.policy'),
  maps = require('../controllers/maps.server.controller');

module.exports = function (app) {
  app.route('/api/maps')
    .get(maps.list)
    .post(maps.create);

  app.route('/api/maps/:mapId')
    .get(maps.read)
    .put(maps.update)
    .delete(maps.delete);

  app.param('mapId', maps.mapByID);
};
